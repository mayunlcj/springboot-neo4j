# springboot-neo4j

#### 介绍
1. 本项目是springboot整合neo4j的最新版，适合初学者。
2. 其中spring-data-neo4j版本是7.2.3,和之前的版本有较大变化，因此重新去官网学习了一下。
3. 代码亲测可用，包含基础的CURD。


#### 使用说明
1. 填写application.yml的相关配置
2. 直接启动springboot程序，访问localhost:8080/movies/Hello有结果说明启动成功。


