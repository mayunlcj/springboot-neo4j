package com.jx.springbootneo4j.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableNeo4jRepositories(basePackages = "com.jx.springbootneo4j.repository")
@EnableTransactionManagement  //激活隐式事务
public class Neo4jConfig {
}
