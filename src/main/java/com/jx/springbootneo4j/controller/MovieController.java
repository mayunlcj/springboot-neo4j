package com.jx.springbootneo4j.controller;

import com.jx.springbootneo4j.entity.MovieEntity;
import com.jx.springbootneo4j.entity.PersonEntity;
import com.jx.springbootneo4j.repository.MovieRepository;
import com.jx.springbootneo4j.repository.PersonRespository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@Slf4j
@RequestMapping("/movies")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private PersonRespository personRespository;

    @GetMapping("Hello")
    String hello(){return "Hello!";}
    @GetMapping("/create")
    public String initMovies(){

        HashSet<PersonEntity> actors_1 = new HashSet<>();
        actors_1.add(PersonEntity.builder().name("Jack").born(1999).build());
        actors_1.add(PersonEntity.builder().name("Rose").born(1998).build());

        HashSet<PersonEntity> actors_2 = new HashSet<>();
        actors_2.add(PersonEntity.builder().name("大鹏").born(1992).build());
        actors_2.add(PersonEntity.builder().name("古力娜扎").born(1993).build());

        HashSet<PersonEntity> directors_1 = new HashSet<>();
        directors_1.add(PersonEntity.builder().name("大鹏").born(1992).build());

        Set<PersonEntity> personEntities = new HashSet<>();
        personEntities.addAll(actors_1);
        personEntities.addAll(actors_2);
        personEntities.addAll(directors_1);
        log.info("entities:" + personEntities);

        personRespository.saveAll(personEntities);


        MovieEntity movieEntity = MovieEntity.builder().title("泰坦尼克号").description("浪漫的爱情故事").
                actors(actors_1).build();
        MovieEntity movieEntity1 = MovieEntity.builder().title("缝纫机乐队").description("热血的戏电影").
                actors(actors_2).directors(directors_1).build();

        List<MovieEntity> movieEntities = Arrays.asList(movieEntity, movieEntity1);
        log.info("entities:" + movieEntities);
        movieRepository.saveAll(movieEntities);

        return "ok";
    }
    @GetMapping("/searchByTitle")
    public MovieEntity searchByTitle(@RequestParam("title")String title){
        MovieEntity movieEntityMono = movieRepository.findOneByTitle(title);
        return movieEntityMono;
    }
    @GetMapping("/searchByKeyWords")
    List<MovieEntity> getMovies(@RequestParam("keywords")String keywords) {
        return movieRepository.findAllByKeyWords(keywords);
    }
//    DELETE /id/123
    @DeleteMapping("/id")
    void  delete(@PathVariable Long id) {
         movieRepository.deleteById(id);
    }

}
