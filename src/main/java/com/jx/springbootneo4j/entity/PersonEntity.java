package com.jx.springbootneo4j.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;

@Data
@Builder
@AllArgsConstructor
@Node("People")
public class PersonEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Property
    private  String name;
    @Property
    private  Integer born;
    //Getters omitted
}