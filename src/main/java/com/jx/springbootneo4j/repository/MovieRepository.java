package com.jx.springbootneo4j.repository;

import com.jx.springbootneo4j.entity.MovieEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface MovieRepository extends Neo4jRepository<MovieEntity,Long> {
//    返回一个或0个结果
     MovieEntity findOneByTitle(String title);
//    返回多个结果
    @Query("MATCH (n:Movie) WHERE n.title CONTAINS $keywords RETURN n")
    List<MovieEntity> findAllByKeyWords(String keywords);

}
