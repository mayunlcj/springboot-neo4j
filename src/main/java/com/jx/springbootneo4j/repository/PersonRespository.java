package com.jx.springbootneo4j.repository;

import com.jx.springbootneo4j.entity.MovieEntity;
import com.jx.springbootneo4j.entity.PersonEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PersonRespository extends Neo4jRepository<PersonEntity,Long> {
    //    返回一个或0个结果
    PersonEntity findOneByName(String name);
}
